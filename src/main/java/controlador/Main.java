package controlador;

import modelo.IModelo;
import vista.VistaGrafica;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class Main {
    public static void main (String[] args) { 
        IModelo modelo = null;
        VistaGrafica vista = null;
        Controlador controlador = new Controlador (modelo, vista);
    }
}