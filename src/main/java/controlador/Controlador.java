package controlador;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import modelo.IModelo;
import modelo.ModeloFichero;
import modelo.Usuario;
import vista.Funciones;
import vista.VistaGrafica;
import vista.VistaGraficaAcerca;
import vista.VistaGraficaAlumno;
import vista.VistaGraficaCurso;
import vista.VistaGraficaMatricula;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class Controlador implements ActionListener, MouseListener {
    IModelo modelo = null;
    VistaGrafica vista = null;
    private Funciones f = new Funciones();
    
    public Controlador (IModelo m, VistaGrafica v) {
        boolean valido = false;
        
        //Validamos el acceso
        while (!valido) {
            valido = validarAcceso();
            if (!valido) {
                f.error("El usuario o la contraseña no coinciden o son erróneos.");
            }
        }
        
        //Asignamos el modelo y la vista
        vista = new VistaGrafica();
        modelo = m;
        
        //Añadimos los listeners
        vista.getAlumno().addActionListener(this);
        vista.getCurso().addActionListener(this);
        vista.getMatricula().addActionListener(this);
        vista.getDoc().addActionListener(this);
        vista.getAcerca().addActionListener(this);
        vista.getSalir().addActionListener(this);
        
        vista.getAlumno().addMouseListener(this);
        vista.getCurso().addMouseListener(this);
        vista.getMatricula().addMouseListener(this);
        vista.getDoc().addMouseListener(this);
        vista.getAcerca().addMouseListener(this);
        vista.getSalir().addMouseListener(this);
  }
    
    //Implementamos los métodos de las interfaces de eventos
    @Override
    public void actionPerformed(ActionEvent e) { 
        Object objeto = e.getSource();
        if (objeto == vista.getSalir()) {
            salir();
        } else if (objeto == vista.getAlumno()) {
            try {
                alumno();
            } catch (IOException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (objeto == vista.getCurso()) {
            try {
                curso();
            } catch (IOException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (objeto == vista.getMatricula()) {
            try {
                matricula();
            } catch (IOException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (objeto == vista.getDoc()) {
            documentacion();
        } else if (objeto == vista.getAcerca()) {
            acerca();
        }
    }
    
    @Override
    public void mouseEntered(MouseEvent e) {
        Object objeto = e.getSource();
        
        if (objeto == vista.getSalir()) {
            vista.getInfo().setText("Salir del programa");
        } else if (objeto == vista.getAlumno()) {
            vista.getInfo().setText("Acceder al menú de alumnos");        
        } else if (objeto == vista.getCurso()) {
            vista.getInfo().setText("Acceder al menú de cursos");
        } else if (objeto == vista.getMatricula()) {
            vista.getInfo().setText("Acceder al menú de matrículas");
        } else if (objeto == vista.getDoc()) {
            vista.getInfo().setText("Acceder a la documentación de la aplicación");
        } else if (objeto == vista.getAcerca()) {
            vista.getInfo().setText("Información sobre la aplicación");
        }
    }
    
    public void mouseExited(MouseEvent e) {
        Object objeto = e.getSource();

        if (objeto == vista.getSalir()) {
            vista.getInfo().setText("MENÚ PRINCIPAL");
        } else if (objeto == vista.getAlumno()) {
            vista.getInfo().setText("MENÚ PRINCIPAL");
        } else if (objeto == vista.getCurso()) {
            vista.getInfo().setText("MENÚ PRINCIPAL");
        } else if (objeto == vista.getMatricula()) {
            vista.getInfo().setText("MENÚ PRINCIPAL");
        } else if (objeto == vista.getDoc()) {
            vista.getInfo().setText("MENÚ PRINCIPAL");
        } else if (objeto == vista.getAcerca()) {
            vista.getInfo().setText("MENÚ PRINCIPAL");
        }
    }
    
    public void mousePressed(MouseEvent e) {
        //No hacer nada
    }
    public void mouseReleased(MouseEvent e) {
        //No hacer nada
    }
    public void mouseClicked(MouseEvent e) {
        //No hacer nada
    }
    
    private void salir() {
        System.exit(0);
    }
    
    private void alumno() throws IOException {
        modelo = new ModeloFichero();
        VistaGraficaAlumno v = new VistaGraficaAlumno();
        ControladorAlumno ca = new ControladorAlumno(modelo, v);
    }
    
    private void curso() throws IOException {
        modelo = new ModeloFichero();
        VistaGraficaCurso v = new VistaGraficaCurso();
        ControladorCurso cc = new ControladorCurso(modelo, v);    
    }
    
    private void matricula() throws IOException {
        modelo = new ModeloFichero();
        VistaGraficaMatricula v = new VistaGraficaMatricula();
        ControladorMatricula cm = new ControladorMatricula(modelo, v);
    }
    
    private void documentacion() {
        String url = "https://docs.google.com/document/d/1pTpLNawKELyIV_Vn_pu5DMgM-HdIpmN13lkvVuf6X3o/edit?ts=5624d784";
        Desktop desktop = Desktop.getDesktop();
        if (desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.getDesktop().browse(new URI(url));
            } catch (URISyntaxException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private void acerca() {
        VistaGraficaAcerca v = new VistaGraficaAcerca();
    }
    
    private boolean validarAcceso() {
        Usuario u = new Usuario();
        //ImageIcon icon = new ImageIcon(getClass().getResource("/imagenes/bloqueo.png"));
        String user = JOptionPane.showInputDialog(null, "Introduce el usuario (root)", "root");
        String pass = JOptionPane.showInputDialog(null, "Introduce la contraseña (pass)", "pass");
        return user.equals(u.getUsuario()) && pass.equals(u.getPassword());
    }
}
