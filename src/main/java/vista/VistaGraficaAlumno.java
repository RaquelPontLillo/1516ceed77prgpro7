package vista;

import javax.swing.*;

public class VistaGraficaAlumno extends JFrame {
    //Definimos los botones de la GUI
    private JButton create = new JButton();
    private JButton read = new JButton();
    private JButton update = new JButton();
    private JButton delete = new JButton();
    private JButton search = new JButton();
    private JButton guardar = new JButton();
    private JButton cancelar = new JButton();
    private JButton salir = new JButton();
    private JButton siguiente = new JButton();
    private JButton anterior = new JButton();
    private JButton primero = new JButton();
    private JButton ultimo = new JButton();
    
    //Definimos las etiquetas de la GUI
    private JLabel id = new JLabel("ID: ", JLabel.RIGHT);
    private JLabel nombre = new JLabel("Nombre: ",  JLabel.RIGHT);
    private JLabel dni = new JLabel("DNI / NIE: ", JLabel.RIGHT);
    private JLabel edad = new JLabel("Edad: ", JLabel.RIGHT);
    private JLabel email = new JLabel("E-mail: ", JLabel.RIGHT);
    private JLabel informacion = new JLabel("MENÚ ALUMNO");
    private JLabel logoAlumno = new JLabel();
    
    //Definimos los campos de texto de la GUI
    private JTextField tid = new JTextField();
    private JTextField tnombre = new JTextField();
    private JTextField tdni = new JTextField();
    private JTextField tedad = new JTextField();
    private JTextField temail = new JTextField();
    
    //Definimos los paneles de la GUI
    private JPanel panel = new JPanel();
    
    public VistaGraficaAlumno() {
        //Llamamos al constructor de la clase JFrame
        super("Menú alumno");
        
        //Configuramos el comportamiento del aspa y aspecto general de la GUI
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e) {
            System.err.println("No se ha podido configurar el Look and Feel escogido");
            e.printStackTrace();
        }
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        //Configuramos los distintos componentes de la GUI
        //CRUD
        create.setToolTipText("Añadir nuevo");
        create.setIcon(new ImageIcon(getClass().getResource("/imagenes/create.png")));
        read.setToolTipText("Leer datos");
        read.setIcon(new ImageIcon(getClass().getResource("/imagenes/read.png")));
        update.setToolTipText("Actualizar información");
        update.setIcon(new ImageIcon(getClass().getResource("/imagenes/update.png")));
        update.setEnabled(false);
        delete.setToolTipText("Borrar información");
        delete.setIcon(new ImageIcon(getClass().getResource("/imagenes/delete.png")));
        delete.setEnabled(false);
        search.setToolTipText("Buscar por ID");
        search.setIcon(new ImageIcon(getClass().getResource("/imagenes/search.png")));
        //Navegación
        primero.setToolTipText("Volver al principio");
        primero.setIcon(new ImageIcon(getClass().getResource("/imagenes/primero.png")));
        primero.setEnabled(false);
        anterior.setToolTipText("Anterior");
        anterior.setIcon(new ImageIcon(getClass().getResource("/imagenes/anterior.png")));
        anterior.setEnabled(false);
        siguiente.setToolTipText("Siguiente");
        siguiente.setIcon(new ImageIcon(getClass().getResource("/imagenes/siguiente.png")));
        siguiente.setEnabled(false);
        ultimo.setToolTipText("Ir al último");
        ultimo.setIcon(new ImageIcon(getClass().getResource("/imagenes/ultimo.png")));
        ultimo.setEnabled(false);
        //Guardar, cancelar, salir
        guardar.setToolTipText("Guardar datos");
        guardar.setIcon(new ImageIcon(getClass().getResource("/imagenes/guardar.png")));
        guardar.setEnabled(false);
        cancelar.setToolTipText("Cancelar");
        cancelar.setIcon(new ImageIcon(getClass().getResource("/imagenes/cancelar.png")));
        cancelar.setEnabled(false);
        salir.setToolTipText("Salir del menú");
        salir.setIcon(new ImageIcon(getClass().getResource("/imagenes/salir.png")));
        //Logo
        logoAlumno.setIcon(new ImageIcon(getClass().getResource("/imagenes/logo_alumno.png")));
        //Datos alumno
        tid.setEnabled(false);
        tnombre.setEnabled(false);
        tdni.setEnabled(false);
        tedad.setEnabled(false);
        temail.setEnabled(false);

        //Definimos el aspecto para nuestro diseño y añadimos los botones y paneles
        panel.setBorder(BorderFactory.createTitledBorder("Datos del alumno"));
        GroupLayout panelLayout = new GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
            panelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(nombre)
                    .addComponent(id)
                    .addComponent(dni)
                    .addComponent(edad)
                    .addComponent(email))
                .addGap(18, 18, 18)
                .addGroup(panelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(temail)
                    .addComponent(tedad)
                    .addComponent(tdni)
                    .addComponent(tid)
                    .addComponent(tnombre, GroupLayout.DEFAULT_SIZE, 181, Short.MAX_VALUE))
                .addContainerGap())
        );
        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(id)
                    .addComponent(tid, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(nombre)
                    .addComponent(tnombre, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(dni)
                    .addComponent(tdni, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(edad)
                    .addComponent(tedad, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(email)
                    .addComponent(temail, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(13, Short.MAX_VALUE))
        );

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(create, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(read, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(update, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(delete, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(search, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE))
                            .addComponent(panel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(primero, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(anterior, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(siguiente, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ultimo, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(31, 31, 31)
                                .addComponent(logoAlumno, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE))
                            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(guardar, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cancelar, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(salir, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE))))
                    .addComponent(informacion))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(informacion)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(create, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                    .addComponent(read, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                    .addComponent(update, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                    .addComponent(delete, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                    .addComponent(search, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(panel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(logoAlumno, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(guardar, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                    .addComponent(cancelar, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                    .addComponent(salir, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                    .addComponent(primero, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                    .addComponent(anterior, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                    .addComponent(siguiente, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ultimo, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(12, Short.MAX_VALUE))
        );
        
        pack();
        
        //Hacemos visible la GUI
        setVisible(true);
    }
    
    //Getters
    public JButton getCreate() {
        return create;
    }
    
    public JButton getRead() {
        return read;
    }
    
    public JButton getUpdate() {
        return update;
    }
    
    public JButton getDelete() {
        return delete;
    }
    
    public JButton getSearch() {
        return search;
    }
    
    public JButton getPrimero() {
        return primero;
    }
    
    public JButton getAnterior() {
        return anterior;
    }
    
    public JButton getSiguiente() {
        return siguiente;
    }
    
    public JButton getUltimo() {
        return ultimo;
    }
    
    public JButton getGuardar() {
        return guardar;
    }
    
    public JButton getCancelar() {
        return cancelar;
    }
    
    public JButton getSalir() {
        return salir;
    }
    
    public JLabel getInfo() {
        return informacion;
    }
    
    public JTextField getTid() {
        return tid;
    }
    
    public JTextField getTnombre() {
        return tnombre;
    }
    
    public JTextField getTdni() {
        return tdni;
    }
    
    public JTextField getTedad() {
        return tedad;
    }
    
    public JTextField getTemail() {
        return temail;
    }
}