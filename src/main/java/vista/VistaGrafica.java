package vista;

import javax.swing.*;
import java.awt.*;

public class VistaGrafica extends JFrame {
    //Definimos los botones de la GUI
    private JButton alumno = new JButton();
    private JButton curso = new JButton();
    private JButton matricula = new JButton();
    private JButton documentacion = new JButton();
    private JButton acerca = new JButton();
    private JButton salir = new JButton();
    
    //Definimos los paneles de la GUI
    private JPanel superior = new JPanel();
    private JPanel central = new JPanel();
    private JPanel inferior = new JPanel();
    
    //Definimos las etiquetas de la GUI
    private JLabel logo = new JLabel();
    private JLabel info = new JLabel();

    public VistaGrafica() {
        //Llamamos al constructor de la clase JFrame
        super("Menú principal");
        
        //Configuramos el tamaño, comportamiento del aspa y aspecto general de la GUI
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e) {
            System.err.println("No se ha podido configurar el Look and Feel escogido");
            e.printStackTrace();
        }
        setSize(800,400);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //Damos valor a los botones de la GUI (icono y tooltip)
        alumno.setToolTipText("Menú alumno");
        alumno.setIcon(new ImageIcon(getClass().getResource("/imagenes/alumno.png")));
        curso.setToolTipText("Menú curso");
        curso.setIcon(new ImageIcon(getClass().getResource("/imagenes/curso.png")));
        matricula.setToolTipText("Menú matricula");
        matricula.setIcon(new ImageIcon(getClass().getResource("/imagenes/matricula.png")));
        documentacion.setToolTipText("Ver documentación del programa");
        documentacion.setIcon(new ImageIcon(getClass().getResource("/imagenes/documentacion.png")));
        acerca.setToolTipText("Acerca de MATRÍCULAS APP. 2015-2016");
        acerca.setIcon(new ImageIcon(getClass().getResource("/imagenes/acerca.png")));
        salir.setToolTipText("Salir del programa");
        salir.setIcon(new ImageIcon(getClass().getResource("/imagenes/cerrar.png")));
        
        //Damos valor a las etiquetas de la GUI
        logo.setIcon(new ImageIcon(getClass().getResource("/imagenes/logo_mayus.png")));
        info.setText("MENÚ PRINCIPAL");
        
        //Definimos los layouts para nuestro diseño y añadimos los botones y paneles
        GridLayout gl = new GridLayout(3, 1, 10, 10);
        setLayout(gl);
        FlowLayout fl = new FlowLayout(FlowLayout.CENTER, 25, 10);
        superior.setLayout(fl);
        superior.add(logo);
        add(superior);
        central.setLayout(fl);
        central.add(alumno);
        central.add(curso);
        central.add(matricula);
        central.add(documentacion);
        central.add(acerca);
        central.add(salir);
        add(central);
        inferior.setLayout(fl);
        inferior.add(info);
        add(inferior);
            
        //Hacemos visible la GUI
        setVisible(true);
    }
    
    //Getters
    public JButton getAlumno() {
        return alumno;
    }
    
    public JButton getCurso() {
        return curso;
    }
    
    public JButton getMatricula() {
        return matricula;
    }
    
    public JButton getDoc() {
        return documentacion;
    }
    
    public JButton getAcerca() {
        return acerca;
    }
    
    public JButton getSalir() {
        return salir;
    }
    
    public JLabel getInfo() {
        return info;
    }
}